<!--
SPDX-FileCopyrightText: 2021 embeach

SPDX-License-Identifier: CC-BY-SA-4.0

Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.3:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/embeach/projeqtor-tutorial.git@master#Projeqtor_Tutorial.md

Note-PCRLT:emb.7:specify-identity-name:embeach
Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/embeach/projeqtor-tutorial.git@master#Projeqtor_Tutorial.md
Note-PCRLT:emb.7:personal-data-policy:name-allowed,abbreviation-allowed
Note-PCRLT:emb.7:copyright-holder-add-remove-modify-group-policy:no-permissions,conditions:personal-data-policy
Note-PCRLT:emb.7:map-copyright-holder-to-git-commits: all commits regarding Projeqtor_Tutorial.md

Note-PCRLT:CC-BY-SA-4.0:map-licenses-to-git-commits: same as "emb.7:map-copyright-holder-to-git-commits"
-->
# Projeqtor Tutorial
## Goal
This tutorial is intended to show some use cases for which projeqtor can be used.  
The example situation is as follows:
A person wants to use projeqtor for project management for professional and private projects. 
However, only in a single user scenario. Team scenarios could be handled in the future.  
Some days of the week should be reserved for work and some days for private projects.
Project management should be flexible for new projects and activities that come up after the original planning.
The estimate of work should be trackable and actual work should be assignable.
Scheduling should be helpful in estimating completion dates.

## User Manual
The tutorial was created with version: projeqtorV9.1.4  
The user manual can be found at:
https://manual.projeqtor.org/html_en

## Notation
First, some clarifications about the notation used.  
"ᐅ `target`" means: left-click `target`, where `target` is the name of the button. If `target` is not visible as a name, it should be mentioned in parentheses or footnotes.  
"ᐅ \[tooltip\] `target`" means: mouse pointer (hover) over `target`, where `target` is the name of the button.  
"`key`: `text value`" means: fill in `text value` in the field labeled with `key` (or select in case of dropdown menu).  
"`key`: `~yes`" means: check the checkbox labeled with `key`.  
"`key`: `~no`" means: uncheck the checkbox labeled with `key`.  
"`group` - `key`: `text value`" means: look for the group `group` and look there for the label `key` and fill in `text value`.  
"ᐅ `search` ᐅ `calendars`" means: search for `calendars` in the menu search box.  
"ᐅ hamburger menu " means: search for a button with three vertical dots (mostly on the right side of the screen) and click.

## Steps
### 1. Installation
See https://www.projeqtor.org/en/product-en/technical-en/64-setup-en/413-product-setup-en  
admin and guest are the only users. (credentials admin, admin). Change password (via browser - login screen -> change password) especially if internet is accessible.  
The tutorial was tested using:  
Linux debian buster in a Qubes 4 standalone VM without internet connection (no network vm).  
To install in debian buster the following steps were used:  
install xampp-linux:  
see https://www.apachefriends.org/faq_linux.html  
after downloading run:  
```console
chmod 755 xampp-linux-*-installer.run  
sudo ./xampp-linux-*-installer.run  
sudo /opt/lampp/manager-linux.run  
```
(start mysql, apache)  

download projeqtorV9.0.4 (or any other version)  
after downloading run:  
```console
sudo cp -r projeqtorV9.0.4 /opt/lampp/htdocs/
cd /opt/lampp/htdocs/
sudo chown -R daemon: projeqtor  
```
use `/opt/lampp/etc/php.ini` (to check php features..)

for later upgrade do the following:  
logout from projeqtor.  
download projeqtorV9.1.4 (or any other latest version)  
```console
sudo cp -r projeqtorV9.1.4 /opt/lampp/htdocs/  
sudo chown -R daemon: projeqtorV9.1.4  
cd /opt/lampp/htdocs/  
sudo cp projeqtorV9.0.4/projeqtor/files/config/parameters.php projeqtorV9.1.4/projeqtor/files/config/  
sudo cp projeqtorV9.0.4/projeqtor/tool/parametersLocation.php projeqtorV9.1.4/projeqtor/tool/  
```
optionally remove the projeqtorV9.0.4 folder.  

for login keepassxc (KeePassXC-2.6.4-x86_64.AppImage) was used as password db.  
as browser librewolf (LibreWolf-85.0.2-1.x86_64.AppImage) was used with addon "KeepassXC-Browser" (1.7.8.1)  

browse to http://localhost/projeqtorV9.1.4/projeqtor  (from there db can be created and afterwards login with admin,admin is possible)

### 2. Adjust global parameters
ᐅ `Configuration` ᐅ  `Global parameters` ᐅ  `Planning`  
`apply strict mode for dependencies`: `No`  
ᐅ `save`
ᐅ `Configuration` ᐅ `Global parameters` ᐅ `Work time`  
`unit for Timesheet (real work)`: `hours`  
`Open Days` - `Saturday`: `open days`  
ᐅ `Configuration` ᐅ `Global parameters` ᐅ `Activity`  
`activate todo list management`: `Yes`  
ᐅ `save`  

## 3. Create custom calendars
ᐅ `search` ᐅ `calendars` ᐅ `Standard calendars`  
ᐅ  `new calendar`  
ᐅ  `Description`   
`name`: `CalWorkOnlyMoTuWe`  
ᐅ  `Detail`  
`off days`: Su,Th,Fr,Sa (checkmarks)  
ᐅ  `save`  

ᐅ  `new calendar`  
ᐅ  `Description`   
`name`: `CalWorkOnlyThFrSa`  
`off days`: Su,Mo,Tu,We (checkmarks)  
ᐅ  `save`  

### 4. Create project manager user `PersMg` and members `PersPro`, `PersPriv`
`Environment` ᐅ `Users` ᐅ `new User`  
`user name`: `PersMg`  
`real name`: `PersMg`  
`profile`: `Project Leader`  
`is a resource`: `~yes`  
`function`: `Manager`  
ᐅ  `save`  
ᐅ  `Details`  
ᐅ  `reset password`  
ᐅ  `save`  
`Resources` ᐅ  `PersMg`  
ᐅ `Description`  
`capacity (FTE)`: `0.0625`  
(this means 0.5h per day if 8h is the normal FTE (default))  
ᐅ `Detail` ᐅ `function and cost` - `add`  
`cost`: `96`€/d (this assumes 12€/h like student rate)  
ᐅ `save`  
`Environment` ᐅ `Users` ᐅ `new User`  
`user name`: `PersPro`  
`real name`: `PersPro`  
`profile`: `Project Member`  
`is a resource`: `~yes`  
`function`: `Developer`  
ᐅ `save`  
ᐅ `details`  
ᐅ `reset password`  
ᐅ `save`  
`Resources` ᐅ `PersPro`  
ᐅ `Description`  
`capacity (FTE)`: `0.9375`  
(this means 0.5h per day reserved for PersMg if 8h is the normal FTE (default))  
`standard calendar`: `CalWorkOnlyMoTuWe`  
ᐅ `Detail` ᐅ `function and cost` - `add`(tooltip-name)  
`cost`: `96`€/d (this assumes 12€/h like student rate)  
ᐅ `save`  
`Environment` ᐅ `Users` ᐅ `new User`  
`user name`: `PersPriv`  
`real name`: `PersPriv`  
`profile`: `Project Member`  
`is a resource`: `~yes`  
`function`: `Developer`  
ᐅ `save`  
ᐅ `details`  
ᐅ `reset password`  
ᐅ `save`  
`Resources` ᐅ `PersPriv`  
ᐅ `Description`  
`capacity (FTE)`: `0.9375`  
(this means 0.5h per day reserved for PersMg if 8h is the normal FTE (default))  
`standard calendar`: `CalWorkOnlyThFrSa`  
ᐅ `Detail` ᐅ `function and cost` - `add`(tooltip-name)  
`cost`: `96`€/d (this assumes 12€/h like student rate)  
ᐅ `save`  
ᐅ `logout`  (click right-left corner (down arrow), then `Disconnection`)  

### 5. Create administrative projects `projects`, `ProjectsPro`, `ProjectsPriv`
The idea is to separate the more private and more professional projects.  
ᐅ `login` with `PersMg` (use login screen - fill in username, password)  
ᐅ  `planning` ᐅ `Create new Item`(tooltip-name) ᐅ `project`  
`name`: `projects`  
`type`: `Administrative`  
`manager`: `PersMg`  
ᐅ `save`  
(its unclear if `projects` is of any help, but it was used in presentation video)  
ᐅ `planning` ᐅ `Create new Item`(tooltip-name) ᐅ `project`  
`name`: `projectsPro`  
`type`: `Administrative`  
`manager`: `PersMg`  
`is sub-project of`: `projects`  
ᐅ `save`  
ᐅ `planning` ᐅ `Create new Item`(tooltip-name) ᐅ `project`  
`name`: `projectsPriv`  
`type`: `Administrative`  
`manager`: `PersMg`  
`is sub-project of`: `projects`  
ᐅ `save`  

### 6. Create collection projects `ProjProColl1`, `ProjPrivColl1`
The idea is to use the collection `ProjProColl1` and `ProjPrivColl1` to hold the project management task for all containing projects. 
This will avoid to measure project management work for each project separately.  
However, to be able to evaluate the estimation and real efford of projectmanagement - the task should end at some time. 
Therefore a new collection would be created to host follow up projects and their management.  
ᐅ `login` with `PersMg`  
ᐅ `planning` ᐅ `Create new Item`(tooltip-name) ᐅ `project`  
`name`: `ProjProColl1`  
`type`: `Time and Materials`  
`manager`: `PersMg`  
`is sub-project of`: `ProjectsPro`  
ᐅ `save`  
ᐅ `planning` ᐅ `Create new Item`(tooltip-name) ᐅ `project`  
`name`: `ProjPrivColl1`  
`type`: `Time and Materials`  
`manager`: `PersMg`  
`is sub-project of`: `ProjectsPriv`  
ᐅ `save`  

### 7. Create `ProjMgPro` (project management task) and 2 real projects in `ProjProColl1` with 2 activities for each
ᐅ `planning` ᐅ `Create new Item`(tooltip-name) ᐅ `activity`  
ᐅ `Description`  
`name`: `ProjMgPro`  
ᐅ `Progress`  
`Steering` - `planning`: `recurring (on weekly basis)`  
ᐅ `save`  
ᐅ `Assignment` - `add`(tooltip-name)  
`resource`: `PersMg`  
`Weekly recurring repartition`: Mo:0.063,Tu:0.063,We:0.063  (there are multiple fields, others are zero)  
ᐅ `save`  

ᐅ `planning` ᐅ `Create new Item`(tooltip-name) ᐅ `project`  
`name`: `ProjPro1`  
`type`: `Time and Materials`  
`manager`: `PersMg`  
ᐅ `save`  
ᐅ `Allocations`  
ᐅ `Allocation to project` - `add`(tooltip-name)  
`resource`: `PersPro`  
`rate`: `50`%  
ᐅ `save`  

ᐅ `planning` ᐅ `Create new Item`(tooltip-name) ᐅ `activity`  
ᐅ `Description`  
`name`: `task1`  
`project`: `ProjPro1`  
ᐅ `Progress`  
`costs and works` - `validated work`: `4`d (this is the estimated effort in person days (as described in ᐅ `Configuration` ᐅ `Global parameters` ᐅ `Work time` `number of hours per day`: `8` (default)). This necessary for monitoring estimated and real work. It will propagate through project and parent projects)  
ᐅ `save`  
ᐅ `Assignment` - `add`(tooltip-name)  
`resource`: `PersPro`  
`rate`: `100`%  
`assigned work`: `4`d  
ᐅ `save`  

ᐅ hamburger menu ᐅ `copy element`  
`Copy name`: `task2`  
`Copy assignements to activities (with assigned work)`: `~yes`  
ᐅ `OK`  
ᐅ `Treatment`  
`status`: ᐅ `recorded`  

ᐅ `planning` ᐅ `ProjPro1`  
ᐅ hamburger menu ᐅ `copy current project`  
`Copy name`: `ProjPro2`  
`Copy project structure (activities & milestones)`: `~yes`  
`Copy allocations to project (and sub-projects)`: `~yes`  
`Copy assignements to activities (with assigned work)`: `~yes`  
ᐅ `OK`  
ᐅ `Progress`  
`status`: ᐅ `recorded`  

### 8. Capacity planning for `ProjProColl1`
ᐅ `planning` ᐅ `calculate capacity planning` (tooltip-name)  
`ProjProColl1`: `~yes`  
`start date`: `6/01/2021` (e.g. current date)  
ᐅ `OK`  
The `ProjMgPro` taks should have a longer bar now.  
### 9. Finishing effort estimation and validate scheduling dates
ᐅ `planning` ᐅ `ProjMgPro`
ᐅ `Progress`  
`costs and works` - `validated work`: `1.125`d (copy from `assigned work`)(this is the estimated effort in person days (as described in ᐅ `Configuration` ᐅ `Global parameters` ᐅ `Work time` `number of hours per day`: `8` (default)). This necessary for monitoring estimated and real work. It will propagate through project and parent projects)  
Usually this is done before assigning resources, but in the case of recurring activities it is easier to do it after.  

ᐅ `planning` ᐅ hamburger menu ᐅ `Validate planning` (tooltip-name)  
`project`: `ProjProColl1` (select)  
ᐅ `OK`  
This will copy the planned dates to the validated dates, which for example will result in red colored bars if the task is not completed after the task end date.  

### 10. Create `ProjMgPriv` (project management task) and 2 real projects in `ProjPrivColl1` with 2 activities for each
proceed as described in 7. (replace `ProjMgPro` with `ProjMgPriv` (Weekly recurring repartition: Th:0.063,Fr:0.063,Sa:0.063), `ProjProColl1` with `ProjPrivColl1` and `PersPro` with `PersPriv`)

### 11. Capacity planning for `ProjPrivColl1` and finishing effort estimation and validate scheduling dates
proceed as described in 8. and 9. (replace `ProjProColl1` with `ProjPrivColl1`, `ProjMgPro` with `ProjMgPriv`)  
(also possible to plan/replan both projects).  
The result is shown in the following screenshot:  
<img src="img/projeqtor_planning_gantt.png"  style="width:90%" >  
Note: right-click on a bar will show the resource allocation per day.  

### 12. Create some reports
ᐅ `Reports` ᐅ `Planning` ᐅ `Monthly planning for a resource`  
`resource`: `~empty`  
`month`: `06`  
`include next month`: `~yes`  
ᐅ `show report` (tooltip-name)  
This will show the report. The planned work can be seen for each resource and each activity.  
ᐅ `include this report in Today page` (tooltip-name)  
This will include the report in the Today page (ᐅ `Today`)  
(optionally) ᐅ `define as favorite` (tooltip-name)   
Then the report would be accessible via ᐅ\[tooltip\]  `Reports`.  
Note: Currently the "ᐅ `export to pdf format` (tooltip-name)" function seems to be disfunctional (cropped right side)  
ᐅ `print report` (tooltip-name) (use pdf printer and use landscape format)  
This is a working way to save as pdf.  
The example pdf report can be seen here: [reports/monthly_planning_for_a_resource1.pdf](reports/monthly_planning_for_a_resource1.pdf)  

ᐅ `Reports` ᐅ `Planning` ᐅ `Work synthesis per activity`  
`project`: `projects`  
ᐅ `show report` (tooltip-name)  
ᐅ `include this report in Today page` (tooltip-name)  
ᐅ `print report` (tooltip-name)  
The example pdf report can be seen here: [reports/work_synthesis_per_activity1.pdf](reports/work_synthesis_per_activity1.pdf)  

### Some notes on the planning strategy
From the report (see work_synthesis_per_activity1.pdf) the planning strategy can be guessed.  
It seems to work like the following:

* proceed in the order listed in ᐅ `Planning` (this means the scheduling can be changed by reordering the tasks, priority changes will probably overide the order, the item Id seems to be not a factor (only for the initial order))  
* if constraints in terms of dependencies between tasks (or milestones) exist, they will be respected first. (this means only tasks possible to start will be considered to start)
* the allocation of resources in projects is to interprete as maximum work **per week** (e.g. 50% means probably that there is an evaluation of how much work the resource is able to provide (given his/her calendar and capacity) (in this week (or maybe this is evaluated only for a normal week)). Then 50% of this work will be tried to be scheduled for this resource in this project. If the time budget is reached it will go on with the next project)
* the assignment of resources in activities is to be interpreted as maximum work **per day** (For example, if two activities are to be performed so that they have the same amount of planned work assigned to them at the end of each day, then the assignment for the resource per activity must be set to 50%.
* the reoccuring tasks seem to be reoccuring until all other (not reoccuring) tasks in the project end.
* the algorithm seems to not start any new task if an already started task can be processed instead (this means there seems to be no "round robin" like scheduling involved)

### Add a dependency
ᐅ `Planning`  
click on the right end of a bar and hold and release on the left end of another bar (there should be a dependency arrow visible). (e.g. end of `ProjPro1` - `task1` to start of `ProjPro1` - `task2`).
The dependency can be adjusted with left click on the arrow. E.g. the delay could be set to positive or negative number.  
The dependency can also be added/modified in ᐅ `Planning` ᐅ `ProjPro1` ᐅ `task1` ᐅ `Dependencies`.  
By adding the dependency the task2 might be moved one day ahead and turn red because the planned end date is later than the validated end date, but this is only the case because the global parameter `apply strict mode for dependencies`: `No` is not yet considered. If a new planning is triggered (with same start date as above) the parameter will be considered again, which will allow the start of a dependent task on the same day the previous task ends.

### Add real work
ᐅ  `logout` (`PersMg`)  
ᐅ  `login` with `PersPro`  
ᐅ  `Follow-up` ᐅ  `Timesheet`  
`week`: `22`  
`ProjPro1` - `task1` - `Tu 01`: `2` (there is a small blue `7.5` in the left corner of the field, that shows the planned work time)  
`ProjPro1` - `task1` - `Tu 02`: `2` (small blue `3.8`)  
ᐅ  `save`  
Note: it seems to be possible to assign real work hours for future days.  
(the purpose of `submit work` and `validate work` needs further investigations)  
ᐅ `Planning`  
the `ProjPro1` - `task1` bar has now a small bar inside that shows the real work or progress. See the screenshot.  
<img src="img/projeqtor_planning_gantt2.png"  style="width:90%" >  

### Replan the schedule
To replan the schedule after `PersPro` has worked on 06/01 and 06/02 only `2` hours instead of `7.5` `PersMg` can do the following.  
ᐅ  `logout` (`PersPro`)  
ᐅ  `login` with `PersMg`  
ᐅ `planning` ᐅ `calculate capacity planning` (tooltip-name)  
`projects`: `~yes`  (other `~no`)
`start date`: `6/03/2021` (two days after the initial start)  
ᐅ `OK`  
The new scheduling will not move task days that have already assigned real work.  
Some tasks are red in the new scheduling because the planned end date is later than the validated end date. See screenshot below.  
<img src="img/projeqtor_planning_gantt3.png"  style="width:90%" >  

### Add another project and replan
Now we want to simulate that another important project needs to be processed in parallel.  
ᐅ `planning` ᐅ `ProjPriv2`  
ᐅ hamburger menu ᐅ `copy current project`  
`Copy name`: `ProjPriv3`  
`Copy project structure (activities & milestones)`: `~yes`  
`Copy allocations to project (and sub-projects)`: `~yes`  
`Copy assignements to activities (with assigned work)`: `~yes`  
ᐅ `OK`  
ᐅ `Progress`  
`status`: ᐅ `recorded`  
Now adjust the allocation rates:  
ᐅ `planning` ᐅ `ProjPriv3`
ᐅ `Allocations`  
`PersPriv` - `edit this allocation` (tooltip-name)  
`rate (%)`: `33`  
ᐅ `planning` ᐅ `ProjPriv1`
ᐅ `Allocations`  
`PersPriv` - `edit this allocation` (tooltip-name)  
`rate (%)`: `33`  
ᐅ `planning` ᐅ `ProjPriv2`
ᐅ `Allocations`  
`PersPriv` - `edit this allocation` (tooltip-name)  
`rate (%)`: `33`  
And replan:  
ᐅ `planning` ᐅ `calculate capacity planning` (tooltip-name)  
`projects`: `~yes`  (other `~no`)
`start date`: `6/03/2021` (two days after the initial start)  
ᐅ `OK`  
The 3 private projects can now be processed pseudo parallel (on a per week level).  
Some tasks are red in the new scheduling because the planned end date is later than the validated end date. See screenshot below.  
<img src="img/projeqtor_planning_gantt4.png"  style="width:90%" >  
### Add a baseline
ᐅ `planning` ᐅ  hamburger menu ᐅ  `save a baseline of planning`  
`project`: `projects`  
`name`: `baseline1`  
ᐅ `OK`  
ᐅ  hamburger menu  
`Show baseline` - `On bottom`: `baseline1`  
A grey bar appears under the actual planning bars. This can be useful to compare the planning with earlier plannings.  

### Add todo list item
(this needs activation - see global parameters section)  
ᐅ `planning` ᐅ  `ProjPro1` - `task1`  
ᐅ  `Details`  
(add the following at the bottom)  
`Todo list` - `name`: `todo1`  
`Todo list` - `priority`: `High priority`  
`Todo list` - `responsible`: `PersPro`  
Now, the status of the todolist item can be set to `in progress` to work on it..  
