<!--
SPDX-FileCopyrightText: 2021 embeach

SPDX-License-Identifier: CC-BY-SA-4.0

Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.3:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/embeach/projeqtor-tutorial.git@master#README.md

Note-PCRLT:emb.7:specify-identity-name:embeach
Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/embeach/projeqtor-tutorial.git@master#README.md
Note-PCRLT:emb.7:personal-data-policy:name-allowed,abbreviation-allowed
Note-PCRLT:emb.7:copyright-holder-add-remove-modify-group-policy:no-permissions,conditions:personal-data-policy
Note-PCRLT:emb.7:map-copyright-holder-to-git-commits: all commits regarding README.md

Note-PCRLT:CC-BY-SA-4.0:map-licenses-to-git-commits: same as "emb.7:map-copyright-holder-to-git-commits"
-->
# Projeqtor Tutorial
The tutorial is intended to show some use cases for which projeqtor can be used.  
This is not a high quality tutorial but should give some hints on how the software can be used.  
The tutorial was created with version: projeqtorV9.1.4  
See [Projeqtor_Tutorial.md](Projeqtor_Tutorial.md)  
See [Projeqtor_Tutorial.html](Projeqtor_Tutorial.html)  
html format was generated with:  
```console
pandoc -f markdown_mmd -o Projeqtor_Tutorial.html Projeqtor_Tutorial.md
sed -i "s/Projeqtor_Tutorial.md/Projeqtor_Tutorial.html/" Projeqtor_Tutorial.html
```

## License
The repository is licensed under CC-BY-SA-4.0  
Files in LICENSES folder are licensed under CC-BY-ND-4.0  
Screenshots are licensed under AGPL-3.0-or-later (due to possible included copyrighted content from ProjeQtOr project)  
The repository is REUSEv3.0 compliant.
